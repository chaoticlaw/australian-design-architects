const gulp          = require('gulp'),
      concat        = require('gulp-concat'),
      sass          = require('gulp-sass'),
      stripCSS      = require('gulp-strip-css-comments'),
      child         = require('child_process'),
      gutil         = require('gulp-util'),
      autoprefixer  = require('gulp-autoprefixer'),
      uglify        = require('gulp-uglify'),
      rename        = require('gulp-rename'),
      notify        = require('gulp-notify'),
      cache         = require('gulp-cache'),
      livereload    = require('gulp-livereload'),
      del           = require('del'),
      copy          = require('gulp-copy'),
      express       = require('express');

const EXPRESS_PORT = 8080;
const EXPRESS_ROOT = '_site/';

const scss_directory = '_css/**/*.?(s)css';
const output_directory = '_site/**';
const html_directory = '*.html';

gulp.task('sass', () => {
  gulp.src( scss_directory )
  .pipe( sass( { 'sourceComments': 'map'} ).on('error', sass.logError) )
  .pipe( autoprefixer( 'last 2 versions' ) )
  .pipe( stripCSS() )
  .pipe( gulp.dest( 'css' ) )
  .pipe( gulp.dest( '_site/css' ))
  .pipe( notify({ message: 'CSS + SCSS (' + scss_directory + ') → /css/ ✓'}));
});

gulp.task('clean', () => {
  return del(['_site']);
});

gulp.task('build:jekyll', () => {
  const jekyll = child.spawn('jekyll', ['build', '--future']);
  
  const jekyllLogger = (buffer) => {
    buffer.toString()
    .split(/\n/)
    .forEach((message) => gutil.log('Jekyll: ' + message));
  }
  
  jekyll.stdout.on('data', jekyllLogger);
  jekyll.stderr.on('data', jekyllLogger);
});

gulp.task('dev:jekyll', () => {
  const jekyll = child.spawn('jekyll', ['serve', '--watch', '--incremental', '--future']);
  
  const jekyllLogger = (buffer) => {
    buffer.toString()
    .split(/\n/)
    .forEach((message) => gutil.log('Jekyll: ' + message));
  }
  
  jekyll.stdout.on('data', jekyllLogger);
  jekyll.stderr.on('data', jekyllLogger);
});

gulp.task( 'serve', () => {
  let server = express();
  server.use( express.static( EXPRESS_ROOT ) );
  server.listen( EXPRESS_PORT );
});

gulp.task('watch', () => {
  livereload.listen();
  
  gulp.watch('_css/*.scss', ['sass']);
  
  //gulp.watch(['*.html', '*/*.html', '*.md', '*/*.md', '!_site/**', '!_site/*/**'], ['jekyll']);
  
  gulp.watch(['_site/**/*.{scss,css,html,png,jpg}']).on('change', livereload.changed );
});

gulp.task( 'build', [ 'clean', 'sass',  'build:jekyll' ]);

gulp.task( 'default', [ 'build', 'serve', 'watch' ] );

gulp.task( 'dev', [ 'clean', 'sass', 'dev:jekyll', 'watch' ] );

