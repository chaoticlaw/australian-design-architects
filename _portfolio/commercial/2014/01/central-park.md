---
layout: portfolio
title: Central Park
category: Commercial
date: 2014-01-12
h_img: /img/photos/CentralPark-01-resized.jpg
h_alt: Central Park Exterior
h_attr: Photo by Minh Nguyen
h_x: 1280
h_y: 720
---

<section class="row">
  <section class="col-xs-12 col-md-6 between-xs">
    <h1>An elegant design.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate modi totam, voluptatum quaerat a aliquid ducimus voluptate magnam. Nihil impedit odit nisi ipsum officia aliquid saepe et dolorem, rem necessitatibus.</p>
    <p>Repellendus architecto aperiam ut modi reiciendis tempora autem ipsa corporis consequatur mollitia ipsam, fuga, nemo hic harum labore quo soluta esse reprehenderit. Quibusdam nostrum soluta consequuntur, atque quam repellendus minima.</p>
    <p>Commodi, repellendus magnam porro doloribus soluta! A doloremque aliquam totam delectus quisquam iusto laudantium ipsa corporis, vero aut repellendus molestiae, minima harum beatae omnis maiores cupiditate eos id ex asperiores!</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/CentralPark-02-resized.jpg' | absolute_url }}" alt="A view of the concourse" width="1280" height="720">
      <!-- Photo by Minh Nguyen -->
    </figure>
  </section>
</section>
<section class="row reverse between-xs">
  <section class="col-xs-12 col-md-6">
    <h1>Built to last</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eum consequuntur nostrum consequatur in at itaque velit nisi odio quia autem, sed, facilis eaque numquam possimus error. Nam ab magnam, veritatis!</p>
    <p>Molestiae saepe dolore ullam qui voluptatum tempora accusantium quibusdam fuga, deserunt iusto ipsum numquam debitis totam facilis amet vel quam obcaecati labore ex, culpa delectus. Mollitia ea, sit inventore commodi.</p>
    <p>Quisquam repellendus nemo atque labore fugit non possimus quia inventore sed pariatur, beatae blanditiis ducimus quaerat minus voluptate deserunt, dolor! Nobis repellat velit perspiciatis illum qui numquam eaque minus distinctio.</p>
    <p>Odio eveniet, accusantium similique maiores dolorem corporis id reiciendis quos aspernatur, excepturi culpa facere. Natus obcaecati quibusdam, porro fugit recusandae odit tempore praesentium. Iure saepe totam temporibus, sit iste nesciunt.</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/CentralPark-03-resized.jpg' | absolute_url }}" alt="Stunning lights adorn the ceiling of the commercial interior" width="1280" height="720">
      <!-- Photo by Minh Nguyen -->
    </figure>
    
  </section>
</section>
