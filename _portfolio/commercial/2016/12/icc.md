---
layout: portfolio
title: International Convention Centre
category: Commercial
date: 2016-12-06
h_img: /img/photos/ICC-01-resized.jpg
h_alt: International Convention Centre Sydney
h_attr: Photo by Minh Nguyen
h_x: 1280
h_y: 960
---

<section class="row">
  <section class="col-xs-12 col-md-6">
    <h1>A Post-Modern Harbourside.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate modi totam, voluptatum quaerat a aliquid ducimus voluptate magnam. Nihil impedit odit nisi ipsum officia aliquid saepe et dolorem, rem necessitatibus.</p>
    <p>Repellendus architecto aperiam ut modi reiciendis tempora autem ipsa corporis consequatur mollitia ipsam, fuga, nemo hic harum labore quo soluta esse reprehenderit. Quibusdam nostrum soluta consequuntur, atque quam repellendus minima.</p>
    <p>Commodi, repellendus magnam porro doloribus soluta! A doloremque aliquam totam delectus quisquam iusto laudantium ipsa corporis, vero aut repellendus molestiae, minima harum beatae omnis maiores cupiditate eos id ex asperiores!</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/ICC-02-resized.jpg' | absolute_url }}" alt="A low angle view of the building and its logo" width="720" height="540">
      <!-- Photo by Minh Nguyen --> 
    </figure>
  </section>
</section>
<section class="row reverse">
  <section class="col-xs-12 col-md-6">
    <h1>A bold statement.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa expedita sit odio quos possimus velit, rerum totam, ullam fuga aspernatur itaque voluptatem corrupti dicta quam error nulla, temporibus officia! Beatae.</p>
    <p>Esse culpa qui magni optio modi, velit quod sit amet neque aut corporis, consectetur inventore. Pariatur dicta nulla rerum, reprehenderit, optio in sit adipisci ipsum aspernatur ut iusto, quis quas.</p>
    <ul class="list">
      <li>Lorem ipsum dolor sit amet, Ab!</li>
      <li>Dicta pariatur rerum accusantium, at voluptatibus minus animi quod consequatur doloremque!</li>
      <li>Hic maiores officia enim iusto autem delectus aut facere laboriosam.</li>
    </ul>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maiores alias est optio inventore, blanditiis impedit laboriosam sit nihil explicabo! Maxime molestiae vel minus cupiditate voluptate quae quisquam quaerat harum esse.</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/ICC-03-resized.jpg' | absolute_url }}" alt="The International Convention Centre during a sunset" width="720" height="540">
      <!-- Photo by Minh Nguyen -->
    </figure>
  </section>
</section>
