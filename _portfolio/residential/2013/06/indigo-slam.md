---
layout: portfolio
title: Indigo Slam
hero: 2016 AIA Robin Boyd Award for Residential Architecture (National)
category: Residential
date: 2013-06-27
h_img: /img/photos/IndigoSlam-01-resized.jpg
h_alt: Indigo Slam
h_attr: Photo by Minh Nguyen
h_x: 1280
h_y: 720
---

<section class="row">
  <section class="col-xs-12 col-md-6">
    <h1>A modern design.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate modi totam, voluptatum quaerat a aliquid ducimus voluptate magnam. Nihil impedit odit nisi ipsum officia aliquid saepe et dolorem, rem necessitatibus.</p>
    <p>Repellendus architecto aperiam ut modi reiciendis tempora autem ipsa corporis consequatur mollitia ipsam, fuga, nemo hic harum labore quo soluta esse reprehenderit. Quibusdam nostrum soluta consequuntur, atque quam repellendus minima.</p>
    <p>Commodi, repellendus magnam porro doloribus soluta! A doloremque aliquam totam delectus quisquam iusto laudantium ipsa corporis, vero aut repellendus molestiae, minima harum beatae omnis maiores cupiditate eos id ex asperiores!</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/IndigoSlam-02-resized.jpg' | absolute_url }}" alt="" width="1280" height="720">
      <!-- Photo by Minh Nguyen -->
    </figure>
  </section>
</section>
