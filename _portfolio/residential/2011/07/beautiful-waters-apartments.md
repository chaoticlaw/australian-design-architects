---
layout: portfolio
title: Beautiful Waters Apartments
hero: UDIA National High Density Housing Award for Excellence 2011
category: Residential
date: 2011-07-16
h_img: /img/photos/apart2.jpg
h_alt: Beautiful Waters Apartments, Coogee
h_attr: Photo provided by ADA
h_x: 500
h_y: 667
---
<section class="row">
  <section class="col-xs-12 col-md-6">
    <h1>Stunning design.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate modi totam, voluptatum quaerat a aliquid ducimus voluptate magnam. Nihil impedit odit nisi ipsum officia aliquid saepe et dolorem, rem necessitatibus.</p>
    <p>Repellendus architecto aperiam ut modi reiciendis tempora autem ipsa corporis consequatur mollitia ipsam, fuga, nemo hic harum labore quo soluta esse reprehenderit. Quibusdam nostrum soluta consequuntur, atque quam repellendus minima.</p>
    <p>Commodi, repellendus magnam porro doloribus soluta! A doloremque aliquam totam delectus quisquam iusto laudantium ipsa corporis, vero aut repellendus molestiae, minima harum beatae omnis maiores cupiditate eos id ex asperiores!</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/apart2.jpg' | absolute_url }}" alt="An apartment building" width="500" height="667">
      <!-- Photo provided by ADA -->
    </figure>
  </section>
</section>