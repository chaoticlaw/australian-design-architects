---
layout: portfolio
title: Blues Apartments
hero: NSW Excellence in Housing Award 2014
category: Residential
date: 2014-11-21
h_img: /img/photos/apart1.jpg
h_alt: Blues Apartments, Forestville
h_attr: Photo provided by ADA
h_x: 800
h_y: 339
---
<section class="row">
  <section class="col-xs-12 col-md-6">
    <h1>Living in life.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate modi totam, voluptatum quaerat a aliquid ducimus voluptate magnam. Nihil impedit odit nisi ipsum officia aliquid saepe et dolorem, rem necessitatibus.</p>
    <p>Repellendus architecto aperiam ut modi reiciendis tempora autem ipsa corporis consequatur mollitia ipsam, fuga, nemo hic harum labore quo soluta esse reprehenderit. Quibusdam nostrum soluta consequuntur, atque quam repellendus minima.</p>
    <p>Commodi, repellendus magnam porro doloribus soluta! A doloremque aliquam totam delectus quisquam iusto laudantium ipsa corporis, vero aut repellendus molestiae, minima harum beatae omnis maiores cupiditate eos id ex asperiores!</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/apart1.jpg' | absolute_url }}" alt="An apartment building" width="{{ page.h_x }}" height="{{ page.h_y }}">
      <!-- {{ page.h_attr }} -->
    </figure>
  </section>
</section>
