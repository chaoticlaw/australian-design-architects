---
layout: portfolio
title: Paradise Apartments
hero: UDIA NSW High Density Award for Excellence 2012
category: Residential
date: 2012-03-20
h_img: /img/photos/apart3.jpg
h_alt: The Paradise Apartments, Potts Point
h_attr: Photo provided by ADA
h_x: 600
h_y: 410
---
<section class="row">
  <section class="col-xs-12 col-md-6">
    <h1>Living in life.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate modi totam, voluptatum quaerat a aliquid ducimus voluptate magnam. Nihil impedit odit nisi ipsum officia aliquid saepe et dolorem, rem necessitatibus.</p>
    <p>Repellendus architecto aperiam ut modi reiciendis tempora autem ipsa corporis consequatur mollitia ipsam, fuga, nemo hic harum labore quo soluta esse reprehenderit. Quibusdam nostrum soluta consequuntur, atque quam repellendus minima.</p>
    <p>Commodi, repellendus magnam porro doloribus soluta! A doloremque aliquam totam delectus quisquam iusto laudantium ipsa corporis, vero aut repellendus molestiae, minima harum beatae omnis maiores cupiditate eos id ex asperiores!</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/apart3.jpg' | absolute_url }}" alt="A round building" width="600" height="410">
      <!-- Photo provided by ADA -->  
    </figure>
  </section>
</section>