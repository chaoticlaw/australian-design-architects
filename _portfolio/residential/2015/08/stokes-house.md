---
layout: portfolio
title: Stokes House
hero: NSW Excellence in Housing Award 2015
category: Residential
date: 2015-08-07
h_img: /img/photos/house1.jpg
h_alt: Stokes House
h_attr: Photo provided by ADA
h_x: 500
h_y: 352
---

<section class="row">
  <section class="col-xs-12 col-md-6">
    <h1>Living in life.</h1>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cupiditate modi totam, voluptatum quaerat a aliquid ducimus voluptate magnam. Nihil impedit odit nisi ipsum officia aliquid saepe et dolorem, rem necessitatibus.</p>
    <p>Repellendus architecto aperiam ut modi reiciendis tempora autem ipsa corporis consequatur mollitia ipsam, fuga, nemo hic harum labore quo soluta esse reprehenderit. Quibusdam nostrum soluta consequuntur, atque quam repellendus minima.</p>
    <p>Commodi, repellendus magnam porro doloribus soluta! A doloremque aliquam totam delectus quisquam iusto laudantium ipsa corporis, vero aut repellendus molestiae, minima harum beatae omnis maiores cupiditate eos id ex asperiores!</p>
  </section>
  <section class="col-xs-12 col-md-6">
    <figure>
      <img src="{{ '/img/photos/house1.jpg' | absolute_url }}" alt="A house" width="500" height="352">
      <!-- Photo provided by ADA -->  
    </figure>
  </section>
</section>
